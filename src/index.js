function handleFetchResponse(response) {
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`)
  }
  return response.text()
}

function parseDate(text) {
  const parsed = new Date(text)
  if (Number.isNaN(parsed.getTime())) {
    throw new Error('Could not parse date')
  }
  return parsed
}

function roundTo (n, places) {
  const divisor = Math.pow(10, places)
  return Math.round(n * divisor) / divisor
}

const fancySetTimeout = (callback, ms) => {
  const timer = {
    active: true,
    cancel: () => clearTimeout(handle)
  }
  const handle = setTimeout(() => {
    callback()
    timer.active = false
  }, ms)
  return timer
}

async function main() {
  const videoPlayer = document.getElementById('video-player')
  const startTime = await fetch('contentmount/start-time.txt')
    .then(handleFetchResponse)
    .then(parseDate)

  const sync = () => {
    const targetTime = Date.now() - startTime.getTime()
    if (targetTime < 0) {
      waitToStart(-targetTime)
      return;
    }
    videoPlayer.playbackRate = 1
    videoPlayer.currentTime = targetTime / 1000.0
    videoPlayer.play()
  }

  let waitStartTimer = null
  const waitToStart = (delay) => {
    if (waitStartTimer !== null) { waitStartTimer.cancel() }
    waitStartTimer = fancySetTimeout(sync, delay)
  }

  const getDeltaMs = () => {
    const targetTimeMs = Date.now() - startTime.getTime()
    const currentTimeMs = videoPlayer.currentTime * 1000
    return targetTimeMs - currentTimeMs
  }

  let catchUpTimer = null
  const catchUp = (rate) => {
    if (catchUpTimer !== null) { catchUpTimer.cancel() }
    videoPlayer.playbackRate = rate
    if (rate !== 1) {
      const delta = getDeltaMs()
      const msUntilCaughtUp = (rate / (rate - 1)) * delta
      catchUpTimer = fancySetTimeout(sync, msUntilCaughtUp)
    }
    videoPlayer.play()
  }

  const status = document.getElementById('status')
  const updateStatus = () => {
    const rate = videoPlayer.playbackRate
    const paused = videoPlayer.paused
    const waitingToStart = waitStartTimer !== null && waitStartTimer.active
    const delta = getDeltaMs()
    let statusText = ''
    if (paused) {
      statusText += 'Paused'
      if (waitingToStart) {
        statusText += '. '
        statusText += `Video will autoplay at ${startTime.toLocaleString()}`
      }
    } else {
      statusText += `Playing at ${rate}x`
    }
    if (delta > 1000) {
      statusText += '. '
      statusText += `You are ${roundTo(delta/1000, 1)} seconds behind.`
    }
    status.innerText = statusText
  }

  videoPlayer.addEventListener('play', updateStatus)
  videoPlayer.addEventListener('pause', updateStatus)
  videoPlayer.addEventListener('ratechange', updateStatus)
  setInterval(updateStatus, 1000)

  document.getElementById('sync').onclick = sync
  document.getElementById('play-10').onclick = () => catchUp(1)
  document.getElementById('catchup-11').onclick = () => catchUp(1.1)
  document.getElementById('catchup-12').onclick = () => catchUp(1.2)
  document.getElementById('catchup-15').onclick = () => catchUp(1.5)
  sync()
}

main().catch(error => document.write(`Error: ${error.message}`))
