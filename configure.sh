#!/bin/sh

set -eu

info () {
  echo "$@" >&2
}

video_path=${1:-}
start_time=${2:-}
subtitles_path=${3:-}

if [ -z "$video_path" ] || [ -z "$start_time" ]; then
  info "Usage: $(basename $0) <path to mp4> <start time as ISO 8601> [<path to VTT subtitle file>]"
  exit 1
fi

static_src="./src"
srv_root="./srv"
content_dir="${srv_root}/contentmount"

read -p "Will overwrite $srv_root. Continue? [y/N] " answer
if [ ! "$answer" = "y" ]; then
  info "Abort"
  exit 1
fi

rm -rf "${srv_root}"
mkdir -p "${srv_root}"
mkdir -p "${content_dir}"
cp -rv "${static_src}/"* "${srv_root}/"
ln -v "$video_path" "${content_dir}/video.mp4"
if [ -n "${subtitles_path}" ]; then
  cp -v "${subtitles_path}" "${content_dir}/subtitles.vtt"
else
  info "Skipping subtitles"
fi
echo -n "${start_time}" > "${content_dir}/start-time.txt"
info "Set start time to ${start_time}"
info "Now run docker-compose up or copy ${srv_root} to your public_html"
