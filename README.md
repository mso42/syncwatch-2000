# SyncWatch 2000

A portable web server for synchronised video watching. Serves only static files using nginx inside Docker, and uses vanilla JS on the client side.

## Requirements
To run the scripts:

- Some kind of *nix system, with a POSIX shell and standard utilities (`cp`, `rm`, `ln`, `mkdir`, etc.)

For transcoding and subtitle extraction:

- FFmpeg, compiled with support for H.264, AAC, MP4 and VTT codecs/containers

To self-host with Docker:

- Docker
- docker-compose

To view the content:

- A web browser that supports HTML5 `<video>`

## Instructions

1. Convert your video file to MP4 (lib264/aac). FFmpeg can do this for you with `ffmpeg -i <input> <output.mp4>`.
2. (Optional) Extract VTT subtitles from your video file, or convert them from an SRT file. FFmpeg can also do this for you with `ffmpeg -i <input vido or srt> <output.vtt>`
3. Figure out your synchronised start time in ISO 8601 format. ISO 8601 looks like `2020-04-25T17:00:00+0100`.
4. Run `./configure.sh <path to output.mp4> <video start time in ISO 8601 format> [<path to subtitles.vtt>`.
5. Run `docker-compose up`
6. Service is running at `http://0.0.0.0:8080/`

## Copyright

Copyright 2020 mso42 42A8B81CAB5FA82C73CF3A4B36E8AC2ABDFCECE9

Released under the GPL v3 or later.

See [LICENSE](./LICENSE) file for license text.
